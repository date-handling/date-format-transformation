package poc.spring.date.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import poc.spring.date.controller.dtos.DateRequest;

@RestController
@RequestMapping("date")
public class DateController {

	@PostMapping
	public Map<String, String> getDateAsUtc(@RequestBody DateRequest request) {

		Map<String, String> map = new TreeMap<String, String>();
		map.put("Epoch", request.getTimestamp().getTime() + "");
		map.put("Timestamp", request.getTimestamp().toString());
		try {
			map.put("Timestamp SYSDATE", toTimestamp().toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// java.util.date 
		map.put("IST Date", request.getDate().toString());
		map.put("UTC", toUTC(request.getDate()));
		map.put("UTC SYSDATE", toUTC());
		map.put("IST ISO-8601", toISO8601(request.getDate(), "IST").toString());
		return map;
	}

	private String toUTC(Date date) {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		df.setTimeZone(tz);
		return df.format(date);
	}
	
	/*
	 * SYSDATE
	 * */
	private String toUTC() {
		Date date = new Date();
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		df.setTimeZone(tz);
		return df.format(date);
	}
	
	private String toISO8601(Date date, String timeZoneId) {
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		sdf.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		String text = sdf.format(date);
		return text;
	}

	/*
	private Date fromUTC(String dateStr) {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		df.setTimeZone(tz);

		try {
			return df.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}
	*/
	private Timestamp toTimestamp() throws ParseException {
		String utc = toUTC();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Date parsedDate = df.parse(utc);
		return new Timestamp(parsedDate.getTime());
	}
	
	public static String fromTimestampToDateFormatUTC(Timestamp timestamp) {
		Date date = new Date(timestamp.getTime());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		return df.format(date);
	}
}
