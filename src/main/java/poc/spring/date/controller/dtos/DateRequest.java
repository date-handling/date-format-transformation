package poc.spring.date.controller.dtos;

import java.sql.Timestamp;
import java.util.Date;

public class DateRequest {
	private Timestamp timestamp;
	
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Date getDate() {
		Date date = new Date(timestamp.getTime());
		return date;
	}
	
	
	
}
