package poc.spring.date.helpers;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.TimeZone;

public class DateHelper {

	private static final String TIMEZONE_UTC = "UTC";
	private static final String DATE_FORMAT_UTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

	/**
	 * Gets the current timestamp in SQL format.
	 *
	 * @return java.sql.Timestamp
	 * @since 1
	 */
	public static Timestamp rightNow() {
		// return new Timestamp(new java.util.Date().getTime());
		String utc = rightNowAsUTC();
		DateFormat df = new SimpleDateFormat(DATE_FORMAT_UTC);
		Date parsedDate;
		try {
			parsedDate = df.parse(utc);
		} catch (Exception e) {
			parsedDate = new Date();
		}
		return new Timestamp(parsedDate.getTime());
	}

	/**
	 * @param unix date from UI is in seconds java expects it in mili seconds this
	 *             method transform unix from UI to java.sql.Timestamp
	 * 
	 * @return
	 */
	public static Timestamp transformUnixToTimestamp(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}

		return new Timestamp(timestamp.getTime() * 1000);
		// return new Timestamp(timestamp.getTime());
	}

	public static Timestamp substractBuissinessDaysToDate(Timestamp date, long days) {

		LocalDate result = date.toLocalDateTime().toLocalDate();
		int substractedDays = 0;
		while (substractedDays < days) {
			result = result.minusDays(1l);
			if (!(result.getDayOfWeek() == DayOfWeek.SATURDAY || result.getDayOfWeek() == DayOfWeek.SUNDAY)) {
				++substractedDays;
			}
		}
		Timestamp t = Timestamp.valueOf(result.atStartOfDay());
		return t;

	}

	public static Boolean before(Date sourceDate, Date targetDate) {
		if (sourceDate == null || targetDate == null) {
			return false;
		}

		if (sourceDate.before(targetDate))
			return true;

		return false;

	}

	private static String rightNowAsUTC() {
		Date date = new Date();
		TimeZone tz = TimeZone.getTimeZone(TIMEZONE_UTC);
		DateFormat df = new SimpleDateFormat(DATE_FORMAT_UTC);
		df.setTimeZone(tz);
		return df.format(date);
	}

	public static String toDateFormatUTC(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat(DATE_FORMAT_UTC);
		try {
			Date date = new Date(timestamp.getTime());
			return df.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static Timestamp fromString(String strDate) {
		SimpleDateFormat df = new SimpleDateFormat(DateHelper.DATE_FORMAT_UTC);
		try {
			Date date = df.parse(strDate);
			return new Timestamp(date.getTime());
		} catch (Exception e) {
			return null;
		}
	}

	public static boolean isDateNotNull(String strDate) {
		if (strDate == null) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean isDateNotNull(Timestamp timestampDate) {
		if (timestampDate == null) {
			return false;
		} else {
			return true;
		}
	}

	public static String toDateFormatUTC(Date date) {
		if (date == null) {
			return null;
		}
		return toDateFormatUTC(new Timestamp(date.getTime()));
	}
	
	public static Date toTimistampFromString(String date) {
		if(date == null) {
			return null;
		}
		Timestamp t = fromString(date);
		if(t ==null) {
			return null;
		}
		return new Date(t.getTime());
	}

}
