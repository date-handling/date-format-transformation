# date-format-transformation

In this project we tried understanding date format and conversion between from date to UTC
and vice-verca

### endpoint

```endpoint
POST http://localhost:8080/date
```

### Request Body

```request
{
	"timestamp":"2020-04-22T21:09:09Z"
}

or

{
	"timestamp":"1587589749000"
}
```

### Response

```res
{
    "Epoch": "1587589749000",
    "IST Date": "Thu Apr 23 02:39:09 IST 2020",
    "IST ISO-8601": "2020-04-23T02:39:09.000+05:30",
    "Timestamp": "2020-04-23 02:39:09.0",
    "Timestamp SYSDATE": "2020-05-03 13:13:00.0",
    "UTC": "2020-04-22T21:09:09Z",
    "UTC SYSDATE": "2020-05-03T13:13Z"
}
```

### Imp Java classes

```java
1. DateController.java
2. poc.spring.date.helpers.DateHelper.java
```